import React from "react";
import Main from "./Containers/main";
import { Navbar } from "./Components/navbar";
import logo from "./logo.svg";
import "./App.css";

function App() {
  return (
    <>
      <Navbar logo={logo} />
      <Main />
    </>
  );
}

export default App;
