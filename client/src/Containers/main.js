import React, { Component } from "react";
import { Card } from "../Components/card";
import { Loader } from "../Components/loader";
import { SummaryTable, CashflowTable } from "../Components/table";
import LineChart from "../Components/chart";

const INITIAL_STATE = { summary: null, cashflow: null, selected: null };
const summaryName = "summary";
const cashflowName = "free-cash-flow";

export default class Main extends Component {
  constructor() {
    super();
    this.state = INITIAL_STATE;
  }
  componentDidMount() {
    this.fetchData(summaryName);
    this.fetchData(cashflowName);
  }

  fetchData = endpoint => {
    fetch(`/${endpoint}`)
      .then(res => res.json())
      .then(endpointData => this.onLoad(endpointData, endpoint));
  };

  onLoad = (endpointData, endpoint) => {
    switch (endpoint) {
      case summaryName:
        this.setState({
          summary: endpointData
        });
        break;
      case "cashflow":
      case cashflowName:
        this.setState({
          cashflow: endpointData
        });
        break;
      case "graph":
        this.setState({
          selected: endpointData
        });
        break;

      default:
        break;
    }
  };

  refresh = cardTitle => {
    if (cardTitle === "cashflow") cardTitle = cashflowName;
    this.onLoad(null, cardTitle);
    this.fetchData(cardTitle);
  };

  filter = selected => {
    let index = this.state.cashflow.data.dcfItems.findIndex(
      o => o.name === selected
    );
    this.setState({ selected: this.state.cashflow.data.dcfItems[index] });
  };

  render() {
    let { summary, cashflow, selected } = this.state;

    return (
      <>
        <div className="columns is-centered">
          <div className="column is-full">
            {summary ? (
              <Card
                title="summary"
                message={summary.message}
                parentCallback={this.refresh}
              >
                <SummaryTable data={summary.data.dcfItems} />
              </Card>
            ) : (
              <Card>
                <Loader active />
              </Card>
            )}
          </div>
        </div>
        <div className="columns">
          <div className="column is-half">
            {cashflow ? (
              <Card
                title="cashflow"
                message={cashflow.message}
                parentCallback={this.refresh}
              >
                <CashflowTable
                  data={cashflow.data.dcfItems}
                  startYear={cashflow.data.startYear}
                  parentCallback={this.filter}
                />
              </Card>
            ) : (
              <Card>
                <Loader active />
              </Card>
            )}
          </div>
          <div className="column is-half">
            <Card
              title="graph"
              message="Select cashflow item to view chart"
              parentCallback={this.filter}
            >
              <LineChart selected={selected} />
            </Card>
          </div>
        </div>
      </>
    );
  }
}
