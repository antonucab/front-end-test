import React from "react";

export const Loader = props => (
  <div className={`loader-wrapper ${props.active ? "is-active" : ""}`}>
    <div className="loader is-loading"></div>
  </div>
);
