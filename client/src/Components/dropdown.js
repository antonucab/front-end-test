import React, { useState } from "react";

export const DropDown = props => {
  const [isActive, setActive] = useState(false);
  const [selected, setSelected] = useState(null);

  return (
    <div className={`dropdown ${isActive ? " is-active" : ""}`}>
      <div className="dropdown-trigger">
        <button
          className="button is-white"
          style={{ marginLeft: "-15px" }}
          aria-haspopup="true"
          aria-controls="dropdown-menu"
          onClick={() => setActive(!isActive)}
        >
          <span>{selected !== null ? selected : props.title}</span>
          <span className="icon is-small">
            <i className="fas fa-angle-down" aria-hidden="true"></i>
          </span>
        </button>
      </div>
      <div className="dropdown-menu" id="dropdown-menu" role="menu">
        <div className="dropdown-content">
          {props.dropdownItems ? (
            props.dropdownItems.map((item, key) => (
              <a
                href="/#"
                className={`dropdown-item ${
                  selected === item ? " is-active" : ""
                }`}
                key={key}
                onClick={e => {
                  e.preventDefault(); //avoid scroll jump
                  setActive(!isActive);
                  setSelected(item);
                  props.parentCallback(item);
                }}
              >
                {item}
              </a>
            ))
          ) : (
            <p className="dropdown-item">loading..</p>
          )}
        </div>
      </div>
    </div>
  );
};
