import React, { useState } from "react";
import { DropDown } from "../Components/dropdown";

export const Table = props => {
  return (
    <table
      className={`table is-fullwidth is-hoverable ${
        props.striped ? "is-striped" : ""
      }`}
    >
      <thead>
        <tr>
          <th>
            {props.dropdownItems ? (
              <DropDown
                title="Name"
                dropdownItems={props.dropdownItems}
                parentCallback={props.parentCallback}
              />
            ) : (
              "Name"
            )}
          </th>
          <th>Year</th>
          <th>Value</th>
        </tr>
      </thead>

      <tbody>{props.children}</tbody>
    </table>
  );
};

export const SummaryTable = props => (
  <Table striped>
    {props.data.map(d => (
      <tr key={d.uid}>
        <td>{d.name}</td>
        <td>{latestYear(d.fact)}</td>
        <td>{d.fact[latestYear(d.fact, "index")].value.toFixed(2)}</td>
      </tr>
    ))}
  </Table>
);

export const CashflowTable = props => {
  const [filter, setFilter] = useState(null);
  let cashFlowNames = [];

  function callback(selectedName) {
    setFilter(selectedName);
    props.parentCallback(selectedName);
  }

  return (
    <Table dropdownItems={cashFlowNames} parentCallback={callback}>
      {props.data.map(d => {
        //basic sorting by year
        d.fact.sort((a, b) => (a.year > b.year ? -1 : 1));
        //simple filtering based on selected name
        cashFlowNames.push(d.name);

        if (filter ? filter === d.name : true) {
          return (
            <React.Fragment key={d.uid}>
              <tr>
                <td>
                  <b>{d.name}</b>
                </td>
                <td></td>
                <td></td>
              </tr>

              {/* the fact object should have index property at the backend to avoid anti-pattern */}
              {d.fact.map((o, i) => (
                <tr
                  key={i}
                  className={o.year > props.startYear ? "coloredTableRow" : ""}
                >
                  <td>{d.name}</td>
                  <td>{o.year}</td>
                  <td>
                    {typeof o.value === "number" ? o.value.toFixed(3) : o.value}
                  </td>
                </tr>
              ))}
            </React.Fragment>
          );
        } else {
          return <tr key={d.uid}></tr>;
        }
      })}
    </Table>
  );
};

export function latestYear(arr, property = "year") {
  let latestYear = Math.max(...arr.map((o, i) => o.year, 0));
  let index = arr.map(o => o.year).indexOf(latestYear);

  return property === "year" ? latestYear : index;
}
