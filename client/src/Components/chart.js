/* eslint-disable react-hooks/exhaustive-deps */
import React, { useRef, useEffect, useState } from "react";
import Chart from "chart.js";
let myLineChart;
const LineChart = props => {
  const [data, setData] = useState(null);
  const canvasRef = useRef(null);

  useEffect(() => {
    setData(props.selected);
    try {
      myLineChart.destroy();
    } catch (error) {
      console.log("chart doesnt exist");
    }
    if (data) {
      data.fact.sort((a, b) => (a.year > b.year ? 1 : -1));
      myLineChart = new Chart(canvasRef.current, {
        type: "line",
        data: {
          labels: data.fact.map(d => d.year),
          datasets: [
            {
              data: data.fact.map(d => d.value),
              backgroundColor: "rgba(138,43,226 ,0.3 )",
              borderColor: "blueviolet"
            }
          ]
        },
        options: {
          responsive: true,
          title: {
            display: true,
            text: data.name,
            fontSize: 16,
            padding: 15
          },
          legend: {
            display: false
          },
          tooltips: {
            enabled: true
          }
        }
      });
    }
  });

  return <canvas ref={canvasRef} />;
};

export default LineChart;
