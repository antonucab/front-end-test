import React from "react";

export const Navbar = props => (
  <nav className="navbar" role="navigation" aria-label="main navigation">
    <div className="navbar-brand">
      <a className="navbar-item" href="https://bulma.io">
        <img src={props.logo} width="112" height="28" alt="valsys_logo" />
      </a>
    </div>
  </nav>
);
