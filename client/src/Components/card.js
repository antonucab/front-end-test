import React from "react";

export const Card = props => (
  <div>
    <div className="card" style={{ minHeight: "450px" }}>
      <div className="card-content">
        <div className="card-title">
          <p className="title">{props.title}</p>
          <button
            className="button is-light"
            onClick={() => props.parentCallback(props.title)}
          >
            refresh
          </button>
        </div>
        <p className="subtitle">{props.message ? props.message : ""}</p>
        {props.children}
      </div>
    </div>
  </div>
);
