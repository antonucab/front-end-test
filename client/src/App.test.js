//Basic component tests
import React from "react";
import { render, fireEvent } from "@testing-library/react";
import { toBeInTheDocument, toHaveClass } from "@testing-library/jest-dom";
import { Card } from "./Components/card";
import { SummaryTable, latestYear } from "./Components/table";
import { DropDown } from "./Components/dropdown";

const fakeData = {
  summary: {
    status: "success",
    message: "Valuation summary",
    data: {
      startYear: 2018,
      uid: "0x3875",
      dcfItems: [
        {
          uid: "0x3799",
          name: "Cash and cash equivalents",
          fact: [
            { year: 2018, value: 8756.3 },
            { year: 2016, value: 2128.8 },
            { year: 2021, value: 17704.133 },
            { year: 2022, value: 21017.883 },
            { year: 2017, value: 2462.3 },
            { year: 2023, value: 24331.633 },
            { year: 2020, value: 14390.383 },
            { year: 2019, value: 11076.633 }
          ],
          order: 5
        }
      ]
    }
  }
};

test("renders Card with title and fake data", () => {
  const { getByText } = render(
    <Card title="summary" message={fakeData.summary.message} />
  );
  expect(getByText("summary")).toBeInTheDocument();
  expect(getByText("Valuation summary")).toBeInTheDocument();
});

test("renders Table with fake data", () => {
  const { getByText, container } = render(
    <SummaryTable data={fakeData.summary.data.dcfItems} />
  );
  expect(getByText("Cash and cash equivalents")).toBeInTheDocument();
  expect(getByText("2023")).toBeInTheDocument();
  expect(getByText("24331.63")).toBeInTheDocument();
});

test("retrieving latest year & index from fake summary data", () => {
  const year = latestYear(fakeData.summary.data.dcfItems[0].fact);
  const index = latestYear(fakeData.summary.data.dcfItems[0].fact, "index");
  expect(year).toEqual(2023);
  expect(index).toEqual(5);
});

test("renders and opens dropdown", () => {
  const { getByText, container } = render(
    <DropDown title="Name" dropdownItems={[1, 2, 3, 4, 5, 6]} />
  );
  expect(getByText("5")).toBeInTheDocument();

  fireEvent.click(getByText("Name"));
  expect(container.firstChild).toHaveClass("is-active");
  fireEvent.click(getByText("Name"));
  expect(container.firstChild).toHaveClass("dropdown");
});
